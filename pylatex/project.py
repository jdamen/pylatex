"""
contains the latex project functionality
"""

from __future__ import print_function

import os
import re
import logging
import time
import shutil
import subprocess
import webbrowser
import datetime
from .utils import createDirIfNotExists, WorkingDir, Timer

from . import settings
SETTINGS = settings.load()
COMPILERS         = SETTINGS['compilers']
OPENERS           = SETTINGS['openers']
BACKUP_EXTENSIONS = SETTINGS['backup_extensions']
COMPILE_ORDERS    = SETTINGS['compile_orders']
if SETTINGS['enable_pylatex']:
    from .pylatex import processor as pylatex


class CompileError(Exception):
    pass


class LatexProject(object):

    warning_pattern = re.compile('^[\w ]+ Warning: .*')
    error_pattern = re.compile('^\!\s+(.*)')

    def __init__(self, directory):
        if not os.path.isdir(directory):
            raise OSError('cannot find directory "{}"'.format(directory))
        self.directory = directory
        self.logger = logging.getLogger(__name__)

    def build(self, fast=False,
              clean=True, open=False,
              log=False, pdfname=None,
              backup=False, compile_order=None, warnings=False):
        """
        build the project pdf using the specified options
        - fast:          use the compile order 'fast', otherwise use the default compile order
        - clean:         store additional files in subdirectory called main
        - open:          open the pdf when completed
        - log:           open the log file when finished
        - pdfname:       allow renaming of the pdf output file
        - backup:        create backup of relevant files in backup directory
        - compile_order: call custom compile order from config file (default='normal')
        - warnings:      show warnings after building"""

        logging.getLogger(__name__).info('Building latex project {name} at "{dir}"'.format(
            name=self.name, dir=self.directory))

        # set pdf name if not defined
        if pdfname is None:
            pdfname = os.path.split(self.directory)[1]

        # run preprocessing functionality
        self.preprocess(backup=backup)

        # start timer
        with Timer() as t:
            try:
                # construct command list
                commands = self._get_commands(fast, compile_order)
                # start compilation using specified commands
                self.compile(commands)
            except CompileError as e:
                # log errors
                self.logger.error(str(e))
                self.logErrors()
            else:
                # run postprocessing functionality
                self.postprocess(pdfname=pdfname, open=open)
                if warnings:
                    # log warnings and errors
                    self.logErrors()
                # log end of build
                self.logger.info('Completed in {:.2f}s as {}.pdf'.format(t.elapsed, pdfname))
            finally:
                if clean:
                    # clean up the directory after building
                    self.clean()
                if log:
                    # open logfile
                    self.open('log')

    def preprocess(self, backup=False):
        """create backup and convert pytex to tex files"""
        self.logger.info('Preparing')

        if backup:
            self.backup()

        if SETTINGS['enable_pylatex']:
            pylatex(self.directory)

    def compile(self, commands):
        """run compiling commands"""
        # restore files from previous cleanup
        self.restore()
        FNULL = open(os.devnull, 'w')
        self.logger.info('Compiling "main"')
        try:
            # run commands in the project directory
            with WorkingDir(self.directory):
                for c in commands:
                    cmd = c + ['main']
                    # run command as subprocess
                    subprocess.Popen(cmd, stdout=FNULL, stderr=FNULL).wait()
                    self.logger.debug('    '+' '.join(cmd))
        except subprocess.CalledProcessError:
            # handle errors
            raise CompileError('latex error during "{}"'.format(' '.join(cmd)))

    def postprocess(self, pdfname=None, open=False):
        """
        Attempt to create pdf file in specified location after compiling
        The pdflatex compilation leaves a pdf file in the project directory.
        This file is moved to the upper directory with the specified name.
        """
        self.logger.info('Postprocessing')

        # compress
        if SETTINGS.get('compress', False):
            from . import compress
            mainfile = 'main.tex'
            outfile = 'main_compressed.tex'
            compress.compress(self.directory, mainfile, outfile)
            self.logger.info('compressed {} into {}'.format(mainfile, outfile))

        # pdf
        if not pdfname:
            pdfname = os.path.split(self.directory)[1]
        pdfloc = os.path.join(os.path.split(self.directory)[0], pdfname+'.pdf')
        # check if pdf exists and remove if possible
        if os.path.isfile(pdfloc):
            try:
                os.remove(pdfloc)
            except OSError:
                self.logger.error('cannot access file {}'.format(pdfloc))
                return False

        # copy pdf to new location
        shutil.copy(self.find('pdf', name='main'), pdfloc)

        if open:
            # open pdf if requested
            self.open_location(pdfloc)

    @property
    def name(self):
        """return project name based on the directory"""
        return os.path.split(self.directory)[1]

    @property
    def dumpdir(self):
        """return the dump directory location"""
        return os.path.join(self.directory, 'main')

    @property
    def backupdir(self):
        """return the backup directory location"""
        return os.path.join(self.directory, 'backup')

    def clean(self):
        """move excessive files to a subdirectory"""
        createDirIfNotExists(self.dumpdir)
        for f in os.listdir(self.directory):
            if os.path.isfile(os.path.join(self.directory, f)):
                filename = os.path.split(f)[1]
                name, ext = os.path.splitext(filename)
                if name == 'main' and ext not in ['.tex', '.pytex', '.pdf', '.log']:
                    shutil.move(os.path.join(self.directory, f),
                                os.path.join(self.dumpdir, filename))

    def clear(self):
        """remove the dump directory with contents"""
        if os.path.isdir(self.dumpdir):
            shutil.rmtree(self.dumpdir)

    def restore(self):
        """restore the contents of the dump directory to the project directory"""
        if not os.path.isdir(self.dumpdir):
            return

        for f in os.listdir(self.dumpdir):
            try:
                shutil.move(os.path.join(self.dumpdir, f),
                            os.path.join(self.directory, f))
            except IOError:
                raise

        shutil.rmtree(self.dumpdir)

    def backup(self):
        """create a backup of the project files"""
        if not os.path.exists(self.backupdir):
            os.makedirs(self.backupdir)
        else:
            for backup_filepath in os.listdir(self.backupdir):
                path = os.path.join(self.backupdir, backup_filepath)
                if os.path.isfile(path):
                    os.remove(path)
                elif os.path.isdir(path):
                    shutil.rmtree(path)

        backup_files = []
        for f in self.gen_filepaths(dump=True, backup=False):
            if os.path.splitext(f)[1] in BACKUP_EXTENSIONS:
                _, name = os.path.split(f)
                newpath = os.path.join(self.directory, 'backup', name)
                shutil.copy(f, newpath)
                backup_files.append(name)

        if os.listdir(self.backupdir):
            logfile = os.path.join(self.backupdir, '.backuplog')
            with open(logfile, 'w') as f:
                f.write('{dt}\nBACKUP FILES\n\t{files}'.format(
                    dt=datetime.datetime.now(),
                    files='\n\t'.join(backup_files)))
        logging.getLogger(__name__).debug('backup completed ({} files)'.format(len(backup_files)))

    def restore_backup(self):
        """restore the backup"""
        try:
            files = os.listdir(self.backupdir)
            with open(os.path.join(self.backupdir, '.backuplog'), 'r') as f:
                f.readline()
                f.readline()
                backupfiles = [line.strip() for line in f]
        except IOError:
            logging.getLogger(__name__).error('cannot restore backup')

        if sorted(files) == sorted(backupfiles):
            self.restore()
            for path in self.gen_filepaths():
                os.remove(path)
            for backupfile in backupfiles:
                shutil.copy(os.path.join(self.backupdir, backupfile),
                            os.path.join(self.directory, backupfile))

    def find(self, ext, name='main'):
        """find a file based on the name and extension"""
        for f in self.gen_filepaths(dump=True, backup=False):
            base, filename = os.path.split(f)
            f_name, f_ext = os.path.splitext(filename)
            if f_ext.strip('.') == ext.strip('.') and f_name == name:
                return f
        raise OSError('file "{}" not found'.format(name+'.'+ext.strip('.')))

    def open(self, ext):
        """open a file or directory"""
        if ext == 'dir':
            self.open_location(self.directory)
        else:
            f = self.find(ext)
            self.open_location(os.path.join(self.directory, f))

    @classmethod
    def open_location(cls, loc):
        """open a location using the specified openers from the config file"""
        if os.path.isfile(loc):
            name, ext = os.path.splitext(loc)
            if ext in OPENERS:
                opener = OPENERS[ext]
            elif ext in ('.log', '.aux', '.bbl', '.blg', '.toc', '.out', '.py', '.pytex', '.tex'):
                opener = OPENERS['text']
            else:
                webbrowser.open(loc)
                return

            cmd = opener+[loc]
            FNULL = open(os.devnull, 'w')
            try:
                subprocess.Popen(cmd, stdout=FNULL, stderr=FNULL)
            except subprocess.CalledProcessError:
                logging.getLogger(__name__).error('ERROR DURING :: ' + ' '.join(cmd))

        elif os.path.isdir(loc):
            FNULL = open(os.devnull, 'w')
            subprocess.Popen(OPENERS['dir']+[loc], stdout=FNULL, stderr=FNULL)

        else:
            raise ValueError('location cannot be opened')

    def gen_filepaths(self, main=True, dump=False, backup=False, files=True, dirs=False):
        """walk all relevant file paths of the project"""
        dirlist = []
        if main:
            dirlist.append(self.directory)
        if dump:
            dirlist.append(self.dumpdir)
        if backup:
            dirlist.append(self.backupdir)

        for d in dirlist:
            if not os.path.isdir(d):
                continue

            for f in os.listdir(d):
                filepath = os.path.join(d, f)
                if files and os.path.isfile(filepath):
                    yield filepath
                elif dirs and os.path.isdir(filepath):
                    yield filepath

    def _get_commands(self, fast=False, compile_order=None):
        """return list of compile commands based on the compile order"""
        if compile_order is None:
            if fast:
                compile_order = 'fast'
            else:
                compile_order = 'normal'
        else:
            if compile_order not in COMPILE_ORDERS:
                logging.getLogger(__name__).error('compile order not found')
                return False

        commands = []
        for cmd in COMPILE_ORDERS[compile_order]:
            commands.append(COMPILERS[cmd])

        return commands

    def logErrors(self, errors=True):
        """process all the errors and warnings in the log file"""
        try:
            with open(self.find('.log'), 'r') as f:
                lastlast, last = None, None
                errshow = 0
                for line in f:
                    line = line.strip()
                    errshow = self.process_log_line(line, last, lastlast, errshow)
                    lastlast, last = last, line
        except Exception as e:
            self.logger.exception(e)

    def process_log_line(self, line, last, lastlast, errshow=0):
        """display an error or warning from the log file"""
        if errshow:
            if line:
                self.logger.warn('   '+line)
            return errshow - 1
        m = self.warning_pattern.match(line)
        if m:
            self.logger.warn(' W  '+line)
            return 0
        elif self.error_pattern.match(line):
            self.logger.warn(' E'+('\n ').join(
                l for l in ('  '+lastlast, '  '+last, '> '+line) if l.strip()))
            return 2
        return 0

def openPDF(directory, name):
    try:
        if not name.endswith('.pdf'):
            name += '.pdf'
        webbrowser.open(os.path.join(directory, name))
    except PermissionError:
        logging.getLogger(__name__).error('file in use')


def openBIB(directory, name, silent=False):
    try:
        if not name.endswith('.bib'):
            name += '.bib'
        LatexProject.open_location(os.path.join(directory, name))
    except PermissionError:
        logging.getLogger(__name__).error('file in use')
