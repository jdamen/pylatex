#!/usr/bin/env python

"""
Module for LaTeX source code processing and project management

A fixed project structure is used with the project contained in a single
directory with the name of the project that should at least contain a main
build file "main.tex". Additional .tex files may be present in the project
directory.
During the building of a latex pdf additional files are created. After the
build has finished the directory is cleaned by moving the extra files to a
subdirectory "main".
"""

from .project import LatexProject, openPDF, openBIB

__author__ = "John Damen"
__credits__ = ["John Damen"]
__licence__ = "GPL"
__version__ = "1.0"
__maintainer__ = "John Damen"
__status__ = "Development"
