"""
contains the python support for latex files
"""

import re
import os
import logging
import subprocess
from .utils import WorkingDir


patterns = dict(script=re.compile(r'^(\s*)%>>>(.*)$'))


class ProcessingException(Exception):
    pass


class Parser(object):
    """
    pylatex Parser
    processes .pytex file and executes included commands
    returns compile-ready tex output
    """

    def __init__(self, directory):
        self.directory = directory

    def parse(self, f):
        return '\n'.join(self._parse(f))

    def _parse(self, f):
        for i, line in enumerate(f):
            line = line.rstrip()
            m = patterns['script'].match(line)
            if m:
                # yield line
                indent, cmd = m.groups()
                for line in self.script(cmd):
                    yield indent+line
                continue
            yield line

    def script(self, cmd):
        with WorkingDir(self.directory):
            # run script
            p = subprocess.Popen(['C:/python33/python.exe', cmd],
                                 stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, stderr = p.communicate()

            if stderr.strip():
                # yield error
                yield '% ERROR WHILE PROCESSING COMMAND "{}"'.format(cmd)

                # display error
                logging.getLogger(__name__).error('STDOUT DURING EXECUTION OF {}:\n{}'.format(
                    cmd,
                    ('  '+line for line in stderr.decode("utf-8").replace('\r', '').rstrip().split('\n'))))
            elif stdout.strip():
                # yield output
                for line in stdout.decode("utf-8").replace('\r', '').rstrip().split('\n'):
                    yield line


def processor(directory):
    """processes all pytex files and writes results to tex files"""
    for f in os.listdir(directory):
        filepath = os.path.join(directory, f)
        if os.path.isfile(filepath):
            ext = os.path.splitext(f)[1]
            # process file if .pytex
            if ext == '.pytex':
                logging.getLogger(__name__).debug('processing as pytex: {}'.format(str(f)))
                # open pytex file
                with open(filepath, 'r') as fh:
                    # parse file
                    content = Parser(directory).parse(fh)
                # open tex file
                with open(os.path.splitext(filepath)[0]+'.tex', 'w') as fh:
                    # write parsed content to file
                    fh.write(content)
