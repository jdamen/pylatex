import os
import json

DEFAULTS = {
    "compilers": {
        "pdflatex": [
            "D:/tex/miktex/bin/pdflatex.exe",
            "-interaction=nonstopmode",
            "-halt-on-error"
        ],
        "bibtex": [
            "D:/tex/miktex/bin/bibtex.exe"
        ],
    },
    "compile_orders": {
        "normal": ["pdflatex", "bibtex", "bibtex", "pdflatex", "pdflatex"],
        "fast": ["pdflatex"]
    },
    "openers": {
        "text": [
            "notepad"
        ],
        ".tex": [
            "notepad"
        ],
        ".bib": [
            "notepad"
        ],
        "dir": [
            "start",
            "notepad"
        ]
    },
    "verbose": False,
    "enable_pylatex": True,
    "backup_extensions": [
        ".pytex",
        ".tex"
    ],
    "compress": False
}

configfile = os.path.join(os.path.split(__file__)[0], 'config.json')


def load():
    if not os.path.isfile(configfile):
        save(DEFAULTS)

    with open(configfile, 'r') as f:
        return json.load(f)


def save(data):
    for k, v in DEFAULTS.items():
        assert k in data, 'missing key {}'.format(k)
        assert type(v) is type(data[k]), 'type {1} required for {0}'.format(k, type(v).__name__)

    with open(configfile, 'w') as f:
        json.dump(data, f, indent='    ')
