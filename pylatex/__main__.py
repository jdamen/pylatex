#!python3
from .__init__ import *

if __name__ == '__main__':
    import sys
    import argparse
    import logging

    parser = argparse.ArgumentParser('latex', description="""Build pdf file from latex projects.
        A latex project is defined as a directory with latex files.
        Compiled PDF files are placed in the parent directory with the project name as default
        The standard compiling process is """)

    parser.add_argument('directory', type=str,
                        help='location of latex project')
    parser.add_argument('--name', dest='pdfname',
                        help='name of the final pdf file (default=projectname)')
    parser.add_argument('--fast', dest='fast', action='store_true',
                        help='only run PDFLATEX once')
    parser.add_argument('--open', dest='open', action='store_true',
                        help='open built pdf file')
    parser.add_argument('--log', dest='log', action='store_true',
                        help='open log file')
    parser.add_argument('--backup', dest='backup', action='store_true',
                        help='create backup of project files \
                              (backup is applied to extensions defined in settings)')
    parser.add_argument('--verbose', '-v', dest='verbose', action='store_true',
                        help='increase verbosity')
    parser.add_argument('--warnings', '-w', dest='warnings', action='store_true',
                        help='show warnings')
    parser.add_argument('--noclean', dest='clean', action='store_false',
                        help='prevent cleanup of the project directory after building')
    parser.add_argument('--compile-order', dest='compile_order',
                        help='name of the compile order that should be used (overrides --fast). The available orders are defined in the config file.')


    kwargs = parser.parse_args(sys.argv[1:]).__dict__

    logformat = '%(message)s'
    if kwargs.pop('verbose', False):
        logging.basicConfig(format=logformat, level=logging.DEBUG)
    else:
        logging.basicConfig(format=logformat, level=logging.INFO)

    LatexProject(kwargs.pop('directory')).build(**kwargs)
