import os
import time


def createDirIfNotExists(d):
    if not os.path.exists(d):
        os.makedirs(d)


class WorkingDir(object):

    def __init__(self, directory):
        self.directory = directory

    def __enter__(self):
        self.oldwd = os.getcwd()
        os.chdir(self.directory)
        return self

    def __exit__(self, *args):
        os.chdir(self.oldwd)


class Timer(object):

    t0 = None

    def start(self):
        self.t0 = time.time()

    def stop(self):
        return self.elapsed

    @property
    def elapsed(self):
        if self.t0 is not None:
            return time.time()-self.t0
        else:
            return None

    def reset(self):
        self.t0 = None

    def __enter__(self):
        self.start()
        return self

    def __exit__(self, *args):
        self.stop()

    def __str__(self):
        if self.elapsed is None:
            return 'timer not started'
        return '{:.2f}s elapsed'.format(self.elapsed)


