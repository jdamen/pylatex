import re
import os
import datetime


class CompressParser(object):

    bibliography_pattern = re.compile(r'\s*\\bibliography\{(.*)\}.*')
    input_pattern = re.compile(r'\s*\\input\{(.*)\}.*')
    include_pattern = re.compile(r'\s*\\include\{(.*)\}.*')
    EOF_pattern = re.compile(r'\s*\\end\{document\}.*')

    def __init__(self, loc, name, **opts):
        self.location = loc
        self.name = name

    def path_by_name(self, name, **kwargs):
        ext = os.path.splitext(name)[1]
        if not ext:
            name = self.find_filename(self.location, name, **kwargs)
        p = os.path.join(self.location, name)
        return p

    def find_filename(self, loc, name, exts=None):
        if exts is not None:
            for e in exts:
                if os.path.isfile(os.path.join(loc, name+e)):
                    return name+e

        for f in os.listdir(loc):
            n, ext = os.path.splitext(f)
            if n == name:
                return f

        raise IOError('file with name {!r} not found'.format(name))

    def parse(self, fh):
        header = '%compressed file {}\n'.format(datetime.datetime.now().strftime('%Y-%m-%d'))
        content = self.parse_lineiter(fh)
        return header+content

    def parse_lineiter(self, li):
        lines = []
        for l in li:
            if l.strip().startswith('%'):
                continue

            if l.strip() == '':
                if not lines or lines[-1].strip() == '':
                    continue

            if self.EOF_pattern.match(l):
                lines.append(l)
                break

            m = self.bibliography_pattern.match(l)
            if m:
                line = self.parse_bibliography(m.group(1))
                lines.append(line)
                continue

            m = self.input_pattern.match(l)
            if m:
                line = self.parse_input(m.group(1))
                lines.append(line)
                continue

            m = self.include_pattern.match(l)
            if m:
                line = self.parse_include(m.group(1))
                lines.append(line)
                continue

            lines.append(l)

        return ''.join(lines)

    def parse_bibliography(self, arg):
        bbl_filename = self.path_by_name(self.name, exts=('.bbl',))
        if not os.path.isfile(bbl_filename):
            raise IOError('.bbl file not found at {!r}'.format(bbl_filename))
        with open(bbl_filename, 'r') as f:
            return self.parse_lineiter(f)

    def parse_input(self, arg):
        with open(self.path_by_name(arg, exts=('.tex','.bbl', '.txt'))) as f:
            return self.parse_lineiter(f)

    def parse_include(self, arg):
        s = '\\clearpage\n'
        with open(self.path_by_name(arg, exts=('.tex','.bbl', '.txt'))) as f:
            s += self.parse_lineiter(f)
        return s


def compress(loc, input_filename, output_filename, **kwargs):
    p = CompressParser(loc, os.path.splitext(input_filename)[0], **kwargs)
    with open(os.path.join(loc, input_filename), 'r') as f_in:
        with open(os.path.join(loc, output_filename), 'w') as f_out:
            f_out.write(p.parse(f_in))


if __name__ == '__main__':
    txt = """\\bibliography{../d}
    \\input{chapters/test}
    \\include{chapters/test}
    """

    for s in txt.splitlines():
        print(CompressParser('.', 'main').parse_line(s), end='')
