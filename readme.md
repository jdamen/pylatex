Pylatex is a module for LaTeX source code processing and project management

# Project
The latex project as used in this module is meant to keep several latex documents organized.

A new project can be constructed by creating a new directory with the project name that contains the main build file called 'main.tex' (or 'main.pytex' if pylatex is enabled).

The standard project structure is shown below. Items marked with a * are created during the building process and are not required for a valid project. Items with a ^ may be added by the user and are also optional.

    /parent directory
      /<project name>
        /main*
          <compilation files>*
        /backup*
        main.tex
        main.pdf*
        main.log*
        <other files and directories>^
      <project name>.pdf*

# Building
When a project is built, various latex files are executed and a pdf is produced. Several arguments may be supplied in the building process as listed below:

- __fast__: use the compile order 'fast', otherwise use the default compile order
- __clean__: store additional files in subdirectory called main
- __open__: open the pdf when completed
- __log__: open the log file when finished
- __pdfname__: allow renaming of the pdf output file
- __backup__: create backup of relevant files in backup directory
- __compile_order__: call custom compile order from config file (default='normal')
- __warnings__: show warnings after building

A simple build can be performed using the following command:

    pylatex <project directory> [OPTIONS]

## Preprocessing

### Backup
If specified, a backup is made of the project files. The backup is placed in a subdirectory of the project named 'backup'. Only the file extensions as specified in the configuration file are copied.

### Pytex files
.pytex files may contain the name of a python script. The stdout of these scripts is inserted into the location where the python script was called. The output is written to a .tex file. This allows dynamic content within the latex code.

A script is called using the following command:

    %>>>scriptname

A possible application for this functionality is to generate dynamic table contents for latex.

__NB__: pylatex must be enabled in the configuration file

## Compiling
The compiler commands as defined in the compile orders (see Configuration) are executed in sequence. If the commands result in an error, the building process is interrupted.

## Postprocessing
The pdf resulting from the compilation step is copied to the parent directory and renamed based on a specified name or the name of the project. If a file with this name exists, an attempt is made to remove it before copying.

If specified, the new pdf file is opened.

# Configuration
Within the pylatex module is a configuration file called 'config.json'. The various configuration options that are available may be specified in this file using the JSON format.

## Compilers
Compilers are commands that are executed during latex compilation. Every command is a list of arguments. All commands are appended with the argument 'main' at compile time to specify the main LaTeX file of the project.

Examples of these compilers are:

    "pdflatex": [
        "pdflatex",
        "-interaction=nonstopmode",
        "-halt-on-error"
    ],
    "bibtex": [
        "bibtex"
    ]

## Compile orders
A compile order is a list of compilers that are executed in sequence. Each order has a name. Default orders are 'fast' and 'normal'. For example:

    "fast": [
        "pdflatex"
    ],
    "normal": [
        "pdflatex",
        "bibtex",
        "bibtex",
        "pdflatex",
        "pdflatex"
    ]

## Openers
Openers are commands that open a file or directory. They are appended with a file or directory name. The name of the opener is the file extension that is connected to it.

There are two special openers:

- __text__ is used as a default for text extensions
- __dir__ is used to open a directory

## Other options
Other configuration options are:

- __verbose__: determines how much information is logged to the screen
- __enable_pylatex__: toggles the pylatex functionality
- __backup_extensions__: extensions that are moved to the backup directory

# Utilities

## Sublime text builder

    {
        "cmd": ["pylatex", "${folder}", "--open", "--verbose"],
        "selector": "text.tex.latex"
    }
